<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <title>Index</title>

  <link rel="stylesheet" media="screen" href="public/style/style.css" />
  <link rel="stylesheet" href="https://opensource.keycdn.com/fontawesome/4.7.0/font-awesome.min.css" integrity="sha384-dNpIIXE8U05kAbPhy3G1cz+yZmTzA6CY8Vg/u2L9xRnHjJiAK76m2BIEaSEV+/aU" crossorigin="anonymous">

</head>

<body>

  <!-- Header de la page web -->
  <header role="header">
    <nav class="menu" role="navigation">
      <div class="inner">
        <div class="m-left">
          <h1 class="logo">Projet Web</h1>
        </div>
        <div class="m-right">
          <a href="index.php" class="m-link"><i class="fa fa-home" aria-hidden="true"></i> Accueil</a>
          <a href="Apropos.php" class="m-link"><i class="fa fa-book" aria-hidden="true"></i> A propos</a>
          <a href="BDD.php" class="m-link"><i class="fa fa-database" aria-hidden="true"></i> BDD</a>
          <?php
            session_start();
		        if(isset($_SESSION['nomUtilisateur'])) {
			        echo '<a href="Panier.php" class="m-link"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Panier</a>';
			        echo '<a href="Connexion.php" class="m-link"><i class="fa fa-user" aria-hidden="true"></i>'.$_SESSION['nomUtilisateur'].'</a>';
		        }
            else {
			        echo '<a href="Connexion.php" class="m-link"><i class="fa fa-user-circle-o" aria-hidden="true"></i> Connexion</a>';
		        }
          ?>

        </div>
        <div class="m-nav-toggle">
          <span class="m-toggle-icon"></span>
        </div>
      </div>
    </nav>
  </header>
  <!-- Fin Header -->
  <!-- Début d'article -->
  <article>
    <div class="innerArticle">
      <?php
        if(isset($_SESSION['nomUtilisateur'])) {
  		    echo '<h2 style="text-align: center;">Bonjour '.$_SESSION['nomUtilisateur'].', Bienvenu sur notre site !</h2></br></br>';
        }
      ?>
      <h3 class="articleIndex">Présentation du site - Posté par Julien Bascouzaraix le 14 janvier 2018</h3>
      <p class="firstArticle"> Ce site fait référence au projet de S3 du module M3104 Prog Web, où nous devions par binôme réaliser un site
        web côté serveur et client, reflétant une base de données et permettant d'effectuer quelques opérations. Sur ce site nous devions intégrer au minimum :</p>
        <ul>
          <li> Une page d'accueil présentant le site, </li>
          <li> Un menu facilitant la navigation dans le site, </li>
          <li> Une page "à propos", décrivant le travail réalisé et précisant la liste des auteurs du site (le binôme), et éventuellement les difficultés rencontrées  </li>
          <li> Un ensemble de pages constituant un catalogue et permettant de parcourir le contenu de la base (par exemple : un lien qui à partir d'une initiale permet d'accéder aux oeuvres d'un compositeur, puis aux albums contenant des enregistrements de ces oeuvres, et enfin aux enregistrement eux-mêmes),  </li>
          <li> Chaque fois que c'est pertinent, affichage de la photo des musiciens ou de la pochette d'un album, affichage d'un contrôle permettant d'écouter l'extrait de l'enregistrement concerné,  </li>
          <li> Une zone sécurisée (donc avec Connexion et suivi de session) permettant de construire un panier d'achat,  </li>
          <li> Depuis la page décrivant un album, un accès aux services Amazon affichant les informations sur l'album (détails, prix, ...) grâce à la valeur contenue dans le champ ASIN de la table Album (en utilisant l'API Amazon ), ou la liste des albums semblables disponibles lorsque ce champ n'est pas renseigné (pas un simple renvoi sur une page !).</li>
        </ul>
        <p class="firstArticle">Pour détailler un peu ces différents point, on peut retrouver le premier point sur cette page même. Pour le menu, vous pouvez déjà le visionné en haut de cette page web. Dans l'onglet 'A propos' vous pourrez lire notre expérience vis à vis de ce projet (ce qu'on a réalisé, les difficultés rencontrées).
        Vous pouvez également visionner tous les travaux avec la base de données dans l'onglet 'BDD'.
        Pour finir si vous voulez en savoir plus ce le binôme qui à réaliser ce site, cliquez sur l'onglet 'l'équipe' </br>
        Bon Visionnage ! </p>

    </div>
  </article>
  <!-- Fin section article -->
  <!-- Début section footer -->
  <footer role="footer" class="footerClass">
    <p style="text-align: center; color: #fff; margin-top: 0px;">© 2018 - Julien Bascouzaraix, Simon Colin</p>
  </footer>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" charset="utf-8"></script>
  <script src="public/js/app.js" charset="utf-8"></script>
</body>
</html>
