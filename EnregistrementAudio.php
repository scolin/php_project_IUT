<?php
  include 'bd.php';
  $stmt = $pdo->prepare("Select Extrait "
          . "From Enregistrement "
          . "Where Code_Morceau=?");
  $stmt->execute(array($_GET['code']));
  $data = $stmt->fetch();
  header("Content-Type: audio/mpeg");
  if(get_current_user() != "scolin002") {
    $musique = pack("H*", $data['Extrait']);
    echo $musique;
  }
  else {
    echo $data['Extrait'];
  }
?>
