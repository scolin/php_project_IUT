<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <title>Panier</title>

  <link rel="stylesheet" media="screen" href="public/style/style.css" />
  <link rel="stylesheet" media="screen" href="public/style/APropos.css" />
  <link rel="stylesheet" href="https://opensource.keycdn.com/fontawesome/4.7.0/font-awesome.min.css" integrity="sha384-dNpIIXE8U05kAbPhy3G1cz+yZmTzA6CY8Vg/u2L9xRnHjJiAK76m2BIEaSEV+/aU" crossorigin="anonymous">
</head>

<body>

  <!-- Header de la page web -->
  <header role="header">
    <nav class="menu" role="navigation">
      <div class="inner">
        <div class="m-left">
          <h1 class="logo">Projet Web</h1>
        </div>
        <div class="m-right">
          <a href="index.php" class="m-link"><i class="fa fa-home" aria-hidden="true"></i> Accueil</a>
          <a href="Apropos.php" class="m-link"><i class="fa fa-book" aria-hidden="true"></i> A propos</a>
          <a href="BDD.php" class="m-link"><i class="fa fa-database" aria-hidden="true"></i> BDD</a>
          <?php
            session_start();
		        if(isset($_SESSION['nomUtilisateur'])) {
			        echo '<a href="Panier.php" class="m-link"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Panier</a>';
			        echo '<a href="Connexion.php" class="m-link"><i class="fa fa-user" aria-hidden="true"></i>'.$_SESSION['nomUtilisateur'].'</a>';
		        }
            else {
			        echo '<a href="Connexion.php" class="m-link"><i class="fa fa-user-circle-o" aria-hidden="true"></i> Connexion</a>';
		        }
          ?>
        </div>
        <div class="m-nav-toggle">
          <span class="m-toggle-icon"></span>
        </div>
      </div>
    </nav>
  </header>
  <!-- Fin Header -->
  <!-- Début d'article -->

  <div class="global">
    <h2> A propos </h2>
    <p> Au bout d'un mois nous sommes parvenus à réaliser : </p>
    <div class="li">
      <li> Une page d'accueil présentant le site "Accueil" </li>
      <li> Un Header (responsive) facilitant la navigation du site </li>
      <li> Une page "à propos" décrivant le travail réalisé et précisant la liste des auteurs du site (le binôme), et les difficultés rencontrées.</li>
      <li> Concernant la Base de données, on peut : Chercher un musicien, accéder à sa page avec ses informations, on peut également accéder à leurs oeuvres. Sur la page oeuvre, on peut avoir les informations nécessaires ainsi que les enregistrements. A noter que toutes les pages de BDD sont responsive</li>
      <li> On a également implémenté, une page de connexion, ou un utilisateur de la base peut se connecter et ainsi se constituer un panier. </li>
    </div>
    <p>Ce qu'on a pas réalisé :</p>
    <div class="li">
      <li> L'API d'AMAZON </li>
    </div>
    <h2>L'équipe</h2>
    <p> Simon Colin : Sur ce site il s'est occupé de la partie php de la base de données ainsi que de l'intégration du front réalisé par Julien</p>
    <p> Julien Bascouzaraix : Il s'est occupé de toute la partie front du site ainsi que de la partie php de la connexion d'un utilisateur sur le site </p>
    <h2 class="DR">Difficultés rencontrées</h2>
    <p> L'une des plus grosses difficultés rencontrées a été de mettre notre site en ligne, nous avons eu beaucoup de problèmes tels que, des images qui ne s'affichaient pas, les extraits audio qui ne se lancaient pas. </p>
    <p> Autre difficulté également, la construction d'un panier pour l'utilisateur. </p>
    <p> Dans la cas de l'API Amazon, nous avons abandonné après de multiples essais. De plus, nous avons beaucoup de retour de nos camarades sur le fait qu'ils n'avaient pas réussi non plus de leur côté. Nous espérons que vous serez compréhensif à l'égard de ce manque de fonctionnalité.</p>
  </div>
  <!-- Fin section article -->
  <!-- Début section footer -->
  <footer role="footer" class="footerClass">
    <p style="text-align: center; color: #fff; margin-top: 0px;">© 2018 - Julien Bascouzaraix, Simon Colin</p>
  </footer>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" charset="utf-8"></script>
  <script src="public/js/app.js" charset="utf-8"></script>
</body>
</html>
