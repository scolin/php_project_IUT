<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <title>Base de donnees</title>

  <link rel="stylesheet" media="screen" href="public/style/style.css"/>
  <link rel="stylesheet" media="screen" href="public/style/musicien.css"/>
  <link rel="stylesheet" href="https://opensource.keycdn.com/fontawesome/4.7.0/font-awesome.min.css" integrity="sha384-dNpIIXE8U05kAbPhy3G1cz+yZmTzA6CY8Vg/u2L9xRnHjJiAK76m2BIEaSEV+/aU" crossorigin="anonymous">
</head>

<body>

  <!-- Header de la page web -->
  <header role="header">
    <nav class="menu" role="navigation">
      <div class="inner">
        <div class="m-left">
          <h1 class="logo">Projet Web</h1>
        </div>
        <div class="m-right">
          <a href="index.php" class="m-link"><i class="fa fa-home" aria-hidden="true"></i> Accueil</a>
          <a href="Apropos.php" class="m-link"><i class="fa fa-book" aria-hidden="true"></i> A propos</a>
          <a href="BDD.php" class="m-link"><i class="fa fa-database" aria-hidden="true"></i> BDD</a>
          <?php
            session_start();
		        if(isset($_SESSION['nomUtilisateur'])) {
			        echo '<a href="Panier.php" class="m-link"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Panier</a>';
			        echo '<a href="Connexion.php" class="m-link"><i class="fa fa-user" aria-hidden="true"></i>'.$_SESSION['nomUtilisateur'].'</a>';
		        }
            else {
			        echo '<a href="Connexion.php" class="m-link"><i class="fa fa-user-circle-o" aria-hidden="true"></i> Connexion</a>';
		        }
          ?>
        </div>
        <div class="m-nav-toggle">
          <span class="m-toggle-icon"></span>
        </div>
      </div>
    </nav>
  </header>
  <!-- Fin Header -->

  <!-- Debut d'article -->
  <article>
    <div id="searchbar">
        <span id="searchText" align="center">Cherchez un musicien</span>
      <form method="get" action="Musicien.php" class="formulaire">
        <input class="champ" type="text" name="nom" />
        <input class="bouton" type="submit" value="" />
      </form>
    </div>
  </article>

  <div id="global">
    <?php
      include 'bd.php';
      $codeMusicien = $_GET['code'];
      $stmt = $pdo->prepare("Select Nom_Musicien, Prenom_Musicien, Annee_Naissance, Annee_Mort, Pays.Nom_Pays, Genre.Libelle_Abrege, Instrument.Nom_Instrument "
                ."From Musicien "
                ."Inner Join Pays On Pays.Code_Pays=Musicien.Code_Pays "
                ."Inner Join Genre On Genre.Code_Genre=Musicien.Code_Genre "
                ."Inner Join Instrument On Instrument.Code_Instrument=Musicien.Code_Instrument "
                ."Where Code_Musicien=? ");
      $stmt->execute(array($codeMusicien));
      $row = $stmt->fetch(PDO::FETCH_ASSOC);

      $nomMus = "";
      if(!empty($row['Prenom_Musicien'])) {
          $nomMus = $nomMus.$row['Prenom_Musicien']." ";
      }
      $nomMus = $nomMus.$row['Nom_Musicien'];
      echo '  <div class="HeaderPage">';
      echo '    <h2 class="nameMusicien">'.$nomMus.'</h2>';
      echo '    <img src="PhotoMusicien.php?code='.$codeMusicien.'" ><br>';
      echo '  </div>';

      echo '  <div class="informationMusicien">';
      echo '    <h2> Informations </h2>';
      if(isset($row['Annee_Naissance'] )) { echo "<p>Année_Naissance : ".$row['Annee_Naissance']."</p>"; }
      if(isset($row['Annee_Mort'] )) { echo "<p>Année_Mort : ".$row['Annee_Mort']."</p>"; }
      if(isset($row['Nom_Pays'] )) { echo "<p>Pays : ".$row['Nom_Pays']."</p>"; }
      if(isset($row['Libelle_Abrege'] )) { echo "<p>Genre : ".$row['Libelle_Abrege']."</p>"; }
      if(isset($row['Nom_Instrument'] )) { echo "<p>Instrument : ".$row['Nom_Instrument']."</p>"; }
      echo '  </div>';

      echo '  <div class="Oeuvre">';
      echo '    <h2>Oeuvres</h2>';
      $stmt = $pdo->prepare("Select DISTINCT Album.Code_Album, Album.Titre_Album, Oeuvre.Titre_Oeuvre "
                ."From Oeuvre "
                ."Inner Join Composer On Composer.Code_Oeuvre=Oeuvre.Code_Oeuvre "
                ."Inner Join Musicien On Musicien.Code_Musicien=Composer.Code_Musicien "
                ."Inner Join Composition_Oeuvre On Composition_Oeuvre.Code_Oeuvre=Oeuvre.Code_Oeuvre "
                ."Inner Join Enregistrement On Enregistrement.Code_Composition=Composition_Oeuvre.Code_Composition "
                ."Inner Join Composition_Disque On Enregistrement.Code_Morceau=Composition_Disque.Code_Morceau "
                ."Inner Join Disque On Composition_Disque.Code_Disque=Disque.Code_Disque "
                ."Inner Join Album On Album.Code_Album=Disque.Code_Album "
                ."Where Musicien.Code_Musicien=? "
                ."Group By Album.Titre_Album, Album.Code_Album, Oeuvre.Titre_Oeuvre ");
      $stmt->execute(array($codeMusicien));
      $cpt = 0;
      while( $row = $stmt->fetch() ) {
        $cpt = $cpt+1;
        echo '  <a href="Album.php?code='.$row['Code_Album'].'"><div class=AlbumOeuvre>';
        echo '<p>'.$row['Titre_Oeuvre']."</p>";
        echo '<img src="PhotoAlbum.php?code='.$row['Code_Album'].'" class="img" />';
        echo '<p>'.$row['Titre_Album']."</p>";
        echo '  </div></a>';
      }
      if($cpt == 0) {
        echo '<div class=AlbumOeuvre>';
        echo '  <p>Ce musicien n\'a pas composé d\'oeuvres</p>';
        echo '</div>';
      }
      echo '  </div >';
    ?>
  </div>

  <!-- Fin section article -->
  <!-- Debut section footer -->
  <footer role="footer" class="footerClass">
    <p style="text-align: center; color: #fff; margin-top: 0px;">© 2018 - Julien Bascouzaraix, Simon Colin</p>
  </footer>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" charset="utf-8"></script>
  <script src="public/js/app.js" charset="utf-8"></script>
</body>
</html>
