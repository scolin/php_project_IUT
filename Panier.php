<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <title>Panier</title>

  <link rel="stylesheet" media="screen" type="text/css" href="public/style/style.css" />
  <link rel="stylesheet" media="screen" type="text/css" href="public/style/panier.css" />
  <link rel="stylesheet" media="screen" type="text/css" href="public/style/main.css">
  <link rel="stylesheet" href="https://opensource.keycdn.com/fontawesome/4.7.0/font-awesome.min.css" integrity="sha384-dNpIIXE8U05kAbPhy3G1cz+yZmTzA6CY8Vg/u2L9xRnHjJiAK76m2BIEaSEV+/aU" crossorigin="anonymous">
</head>

<body>

  <!-- Header de la page web -->
  <header role="header">
    <nav class="menu" role="navigation">
      <div class="inner">
        <div class="m-left">
          <h1 class="logo">Projet Web</h1>
        </div>
        <div class="m-right">
          <a href="index.php" class="m-link"><i class="fa fa-home" aria-hidden="true"></i> Accueil</a>
          <a href="Apropos.php" class="m-link"><i class="fa fa-book" aria-hidden="true"></i> A propos</a>
          <a href="BDD.php" class="m-link"><i class="fa fa-database" aria-hidden="true"></i> BDD</a>
          <?php
          session_start();
          if(isset($_SESSION['nomUtilisateur'])) {
            echo '<a href="Panier.php" class="m-link"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Panier</a>';
            echo '<a href="Connexion.php" class="m-link"><i class="fa fa-user" aria-hidden="true"></i>'.$_SESSION['nomUtilisateur'].'</a>';
          }
          else {
            echo '<a href="Connexion.php" class="m-link"><i class="fa fa-user-circle-o" aria-hidden="true"></i> Connexion</a>';
          }
          ?>
        </div>
        <div class="m-nav-toggle">
          <span class="m-toggle-icon"></span>
        </div>
      </div>
    </nav>
  </header>
  <!-- Fin Header -->
  <!-- Debut d'article -->
  <article class="panier">
    <div class="HautPanier">
      <p class="pBorder"> Solide Panier :</p>
    </div>
    <div class="Main">
      <div class="limiter">
        <div class="container-table100">
          <div class="wrap-table100">
            <div class="table100">
              <table>
                <thead>
                  <tr class="table100-head">
                    <th class="column1">Titre Album</th>
                    <th class="column2">ID Album</th>
                    <th class="column3">Nom Editeur</th>
                    <th class="column4">Quantite</th>
                    <th class="column5">Prix</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  include 'bd.php';
                  if(isset($_SESSION['panier'][0])) {
                    $quantite = array();
                    for ($i = 0; $i < count($_SESSION['panier']); $i = $i+1) {
                      $elem = $_SESSION['panier'][$i];
                      if(!isset($quantite[$elem])) {
                        $quantite[$elem] = 1;
                      }
                      else {
                        $quantite[$elem] = $quantite[$elem]+1;
                      }
                    }
                    foreach ($quantite as $key => $value) {
                      $stmt = $pdo->prepare("Select Album.Titre_Album, Editeur.Nom_Editeur "
                      ."From Album "
                      ."Inner Join Genre On Genre.Code_Genre=Album.Code_Genre "
                      ."Inner Join Editeur On Editeur.Code_Editeur=Album.Code_Editeur "
                      ."Where Code_Album=? ");
                      $stmt->execute(array($key));
                      $row = $stmt->fetch();
                      echo '<tr>'
                      .'<th class="column1">'.$row['Titre_Album'].'</th>'
                      .'<th class="column2">'.$key.'</th>'
                      .'<th class="column3">'.$row['Nom_Editeur'].'</th>'
                      .'<th class="column4">'.$value.'</th>'
                      .'<th class="column5">'.rand(5, 30).'€</th>'
                      .'</tr>';
                    }
                  }
                  ?>
                  <tr>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </article>
  <!-- Fin section article -->
  <!-- Debut section footer -->
  <footer role="footer" class="footerClass">
    <p style="text-align: center; color: #fff; margin-top: 0px;">© 2018 - Julien Bascouzaraix, Simon Colin</p>
  </footer>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" charset="utf-8"></script>
  <script src="public/js/app.js" charset="utf-8"></script>
</body>
</html>
