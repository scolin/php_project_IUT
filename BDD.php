<?php
  session_start();
  if (!isset($_SESSION["nomUtilisateur"])) {
	 $url = $_SERVER["REQUEST_URI"];
	 header("Location: Connexion.php?url=".$url);
  }
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <title>Base de données</title>

  <link rel="stylesheet" media="screen" href="public/style/style.css"/>
  <link rel="stylesheet" media="screen" href="public/style/rechercheMusicien.css" />
  <link rel="stylesheet" href="https://opensource.keycdn.com/fontawesome/4.7.0/font-awesome.min.css" integrity="sha384-dNpIIXE8U05kAbPhy3G1cz+yZmTzA6CY8Vg/u2L9xRnHjJiAK76m2BIEaSEV+/aU" crossorigin="anonymous">
</head>

<body>

  <!-- Header de la page web -->
  <header role="header">
    <nav class="menu" role="navigation">
      <div class="inner">
        <div class="m-left">
          <h1 class="logo">Projet Web</h1>
        </div>
        <div class="m-right">
          <a href="index.php" class="m-link"><i class="fa fa-home" aria-hidden="true"></i> Accueil</a>
          <a href="Apropos.php" class="m-link"><i class="fa fa-book" aria-hidden="true"></i> A propos</a>
          <a href="BDD.php" class="m-link"><i class="fa fa-database" aria-hidden="true"></i> BDD</a>
          <?php
		        if(isset($_SESSION['nomUtilisateur'])) {
			        echo '<a href="Panier.php" class="m-link"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Panier</a>';
			        echo '<a href="Connexion.php" class="m-link"><i class="fa fa-user" aria-hidden="true"></i>'.$_SESSION['nomUtilisateur'].'</a>';
		        }
            else {
			        echo '<a href="Connexion.php" class="m-link"><i class="fa fa-user-circle-o" aria-hidden="true"></i> Connexion</a>';
		        }
          ?>
        </div>
        <div class="m-nav-toggle">
          <span class="m-toggle-icon"></span>
        </div>
      </div>
    </nav>
  </header>
  <!-- Fin Header -->

  <!-- Début d'article -->
  <article>
    <div id="searchbar">
        <span id="searchText" align="center">Cherchez un musicien</span>
      <form method="get" action="Musicien.php" class="formulaire">
        <input class="champ" type="text" name="nom" />
        <input class="bouton" type="submit" value="" />
      </form>
    </div>
</article>

  <div id="results">
    <?php
      include 'bd.php';

      $stmt = $pdo->prepare("Select TOP 20 Code_Musicien, Prenom_Musicien, Nom_Musicien "
                  ."From Musicien ");
      $stmt->execute();
      while( $row = $stmt->fetch() ) {
        echo '<a class="resultText" href="Details.php?code='.$row['Code_Musicien'].'">'.'<div class="searchResult">';
        echo '<p><i class="fa fa-music" aria-hidden="true"></i>';
        echo ' '.$row['Nom_Musicien'];
        // if(isset($row['Prénom_Musicien'])) {
        // if($row['Prénom_Musicien']!=null && $row['Prénom_Musicien']!="") {
          echo ' '.$row['Prenom_Musicien'];
        // }
        echo '</p>';
        echo '</div></a>';
      }
    ?>
  </div>

  <!-- Fin section article -->
  <!-- Début section footer -->
  <footer role="footer" class="footerClass">
    <p style="text-align: center; color: #fff; margin-top: 0px;">© 2018 - Julien Bascouzaraix, Simon Colin</p>
  </footer>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" charset="utf-8"></script>
  <script src="public/js/app.js" charset="utf-8"></script>
</body>
</html>
