<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <title>Base de données</title>

  <link rel="stylesheet" media="screen" href="public/style/style.css"/>
  <link rel="stylesheet" media="screen" href="public/style/album.css"/>
  <link rel="stylesheet" href="https://opensource.keycdn.com/fontawesome/4.7.0/font-awesome.min.css" integrity="sha384-dNpIIXE8U05kAbPhy3G1cz+yZmTzA6CY8Vg/u2L9xRnHjJiAK76m2BIEaSEV+/aU" crossorigin="anonymous">
</head>

<body>

  <!-- Header de la page web -->
  <header role="header">
    <nav class="menu" role="navigation">
      <div class="inner">
        <div class="m-left">
          <h1 class="logo">Projet Web</h1>
        </div>
        <div class="m-right">
          <a href="index.php" class="m-link"><i class="fa fa-home" aria-hidden="true"></i> Accueil</a>
          <a href="Apropos.php" class="m-link"><i class="fa fa-book" aria-hidden="true"></i> A propos</a>
          <a href="BDD.php" class="m-link"><i class="fa fa-database" aria-hidden="true"></i> BDD</a>
          <?php
            session_start();
		        if(isset($_SESSION['nomUtilisateur'])) {
			        echo '<a href="Panier.php" class="m-link"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Panier</a>';
			        echo '<a href="Connexion.php" class="m-link"><i class="fa fa-user" aria-hidden="true"></i>'.$_SESSION['nomUtilisateur'].'</a>';
		        }
            else {
			        echo '<a href="Connexion.php" class="m-link"><i class="fa fa-user-circle-o" aria-hidden="true"></i> Connexion</a>';
		        }
          ?>
        </div>
        <div class="m-nav-toggle">
          <span class="m-toggle-icon"></span>
        </div>
      </div>
    </nav>
  </header>
  <!-- Fin Header -->
  <?php
    if(isset($_SESSION['nomUtilisateur'])) {
      echo '<a href="AjoutPanier.php?code='.$_GET['code'].'&url='.$_SERVER['REQUEST_URI'].'" class="button"><span class="add"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Ajouter au panier !</span></a>';
    }
  ?>

  <div id="global">
    <?php
      include 'bd.php';
      $n1 = $_GET['code'];

      $codeAlbum = $_GET['code'];
      $stmt = $pdo->prepare("Select Album.Titre_Album, Album.Annee_Album, Genre.Libelle_Abrege, Editeur.Nom_Editeur "
                ."From Album "
                ."Inner Join Genre On Genre.Code_Genre=Album.Code_Genre "
                ."Inner Join Editeur On Editeur.Code_Editeur=Album.Code_Editeur "
                ."Where Code_Album=? ");
      $stmt->execute(array($codeAlbum));
      $row = $stmt->fetch();
      echo '<h1>──── '.$row['Titre_Album'].' ────</h1>';
      // echo '<form action="'.$_SERVER['REQUEST_URI'].'" method="post">';
      // echo '  <input type="submit" name="ajoutPanier" value="Ajouter Au Panier" />';
      // echo '</form>';
      if(isset($row['Annee_Album'] )) { echo "<p>Année de sortie : ".$row['Annee_Album']."</p>"; }
      if(isset($row['Libelle_Abrege'] )) { echo "<p>Genre : ".$row['Libelle_Abrege']."</p>"; }
      if(isset($row['Nom_Editeur'] )) { echo "<p>Editeur : ".$row['Nom_Editeur']."</p>"; }
      echo '<p><img src="PhotoAlbum.php?code='.$codeAlbum.'" /></p>';

      $stmt = $pdo->prepare("Select DISTINCT Enregistrement.Code_Morceau, Enregistrement.Titre  "
                ."From Album "
                ."Inner Join Disque On Album.Code_Album=Disque.Code_Album "
                ."Inner Join Composition_Disque On Composition_Disque.Code_Disque=Disque.Code_Disque "
                ."Inner Join Enregistrement On Enregistrement.Code_Morceau=Composition_Disque.Code_Morceau "
                ."Where Album.Code_Album=? ");
      $query = $stmt->execute(array($codeAlbum));

      echo '<h1>──── Enregistrements ────</h1>';
      while( $row = $stmt->fetch() ) {
        echo '<p>'.$row['Titre'].'</p>';
        echo '<p><audio controls="controls" preload="none">'
                    .'<source src="EnregistrementAudio.php?code='.$row['Code_Morceau'].'" type="audio/mp3" />'
                    .'Il se pourrait que votre navigateur ne soit pas compatible'
                    .'</audio></p>';
      }
    ?>
  </div>
  <!-- Début section footer -->
  <footer role="footer" class="footerClass">
    <p style="text-align: center; color: #fff; margin-top: 0px;">© 2018 - Julien Bascouzaraix, Simon Colin</p>
  </footer>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" charset="utf-8"></script>
  <script src="public/js/app.js" charset="utf-8"></script>
</body>
</html>
