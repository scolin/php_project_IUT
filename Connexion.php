<?php
session_start();
if(isset($_SESSION['nomUtilisateur'])) {
  unset($GLOBALS['nomUtilisateur']);
  session_destroy();
}
else {
  $userLog = "";
  $pass = "";
  $cpt = -1;
  if($_SERVER['REQUEST_METHOD'] == "POST") {
    if(isset($_POST['login'])) { $userLog = $_POST['login']; }
    if(isset($_POST['pwd'])) { $pass = $_POST['pwd']; }
    include 'bd.php';
    $stmt = $pdo->prepare("Select COUNT(*) as Compteur "
                          ."From Abonne "
                          ."Where login = '".$userLog."' And password = '".$pass."'");

    $stmt->execute();
    $cpt = $stmt->rowCount();
    $array = $stmt->fetch();
    $cpt = $array['Compteur'];
  }
  if($cpt >= 1) {
    session_start();
    $_SESSION['nomUtilisateur'] = $userLog;
    if(isset($_GET['url'])) {
       header('Location: '.$_GET['url']);
    }
    else {
       header('Location: index.php');
    }
  }
}
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <title>Connexion</title>

  <link rel="stylesheet" media="screen" href="public/style/style.css" />
  <link rel="stylesheet" media="screen" href="public/style/connexion.css" />
  <link rel="stylesheet" href="https://opensource.keycdn.com/fontawesome/4.7.0/font-awesome.min.css" integrity="sha384-dNpIIXE8U05kAbPhy3G1cz+yZmTzA6CY8Vg/u2L9xRnHjJiAK76m2BIEaSEV+/aU" crossorigin="anonymous">

</head>

<body>

  <!-- Header de la page web -->
  <header role="header">
    <nav class="menu" role="navigation">
      <div class="inner">
        <div class="m-left">
          <h1 class="logo">Projet Web</h1>
        </div>
        <div class="m-right">
          <a href="index.php" class="m-link"><i class="fa fa-home" aria-hidden="true"></i> Accueil</a>
          <a href="Apropos.php" class="m-link"><i class="fa fa-book" aria-hidden="true"></i> A propos</a>
          <a href="BDD.php" class="m-link"><i class="fa fa-database" aria-hidden="true"></i> BDD</a>
          <?php
		        if(isset($_SESSION['nomUtilisateur'])) {
			        echo '<a href="Panier.php" class="m-link"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Panier</a>';
			        echo '<a href="Connexion.php" class="m-link"><i class="fa fa-user" aria-hidden="true"></i>'.$_SESSION['nomUtilisateur'].'</a>';
		        }
            else {
			        echo '<a href="Connexion.php" class="m-link"><i class="fa fa-user-circle-o" aria-hidden="true"></i> Connexion</a>';
		        }
          ?>
        </div>
        <div class="m-nav-toggle">
          <span class="m-toggle-icon"></span>
        </div>
      </div>
    </nav>
  </header>
  <!-- Fin Header -->

  <div class="Connexion">
    <?php

    if(!isset($_SESSION['nomUtilisateur'])) {
      if(isset($_GET['url'])) {
        echo 'Vous devez être connecté pour accéder à cette page';
      }
      echo '<form action="'.$_SERVER["REQUEST_URI"].'" method="post">';
	    echo '<p class"Login">Login</p><br/><input type="text" name="login">';
	    echo '<br/>';
	    echo '<p class"mdp">Mot de passe</p><br/><input type="password" name="pwd"><br/>';
	    echo '<br/>';
	    echo '<input type="submit" value="Connexion">';
	    echo '</form>';
    }
    else {
      echo '<form action="'.$_SERVER["REQUEST_URI"].'" method="post">';
	    echo ' <input type="submit" value="Deconnexion">';
	    echo '</form>';
    }
    ?>
  </div>

  <!-- Debut section footer -->
  <footer role="footer" class="footerClass">
    <p style="text-align: center; color: #fff; margin-top: 0px;">© 2018 - Julien Bascouzaraix, Simon Colin</p>
  </footer>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" charset="utf-8"></script>
  <script src="public/js/app.js" charset="utf-8"></script>
</body>
</html>
